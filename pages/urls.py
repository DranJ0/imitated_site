# pages/urls.py
from django.urls import path
from .views import AboutPageView, NotesPageView, WorkPageView

urlpatterns = [
    path("", AboutPageView.as_view(), name="about"),
    path("work/", WorkPageView.as_view(), name="work"),
    path("notes/", NotesPageView.as_view(), name="notes"),
]
