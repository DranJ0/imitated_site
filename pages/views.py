# pages/views.py
from django.views.generic import TemplateView


class NotesPageView(TemplateView):
    template_name = "notes.html"


class WorkPageView(TemplateView):  # new
    template_name = "work.html"


class AboutPageView(TemplateView):  # new
    template_name = "about.html"
